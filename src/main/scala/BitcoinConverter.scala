/**
  * @author Rabit Ljatifi
  * Matrikelnummer: 1147409
  */

import akka.actor.{Actor, ActorLogging, ActorRef, Props}

object BitcoinConverter {
  def props(printerActor: ActorRef): Props = Props(new BitcoinConverter(printerActor))
  case class bitcoin2euro(bitcoin: BigDecimal)
  case class bitcoin2dollar(bitcoin: BigDecimal)
}

class BitcoinConverter(printerActor: ActorRef) extends Actor with ActorLogging {
  import BitcoinConverter._
  import Printer._

  def receive = {
    case bitcoin2euro(bitcoin: BigDecimal) => val bitcoin2euro = bitcoin * 5551
      printerActor ! PrintCurrency(bitcoin2euro)
    case bitcoin2dollar(bitcoin: BigDecimal) => val bitcoin2dollar = bitcoin * 6500
      printerActor ! PrintCurrency(bitcoin2dollar)
    case _ => println("Error in BitcoinConverter")
  }
}