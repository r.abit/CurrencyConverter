/**
  * @author Rabit Ljatifi
  * Matrikelnummer: 1147409
  */

import akka.actor.{Actor, ActorLogging, ActorRef, Props}

object DollarConverter {
  def props(printerActor: ActorRef): Props = Props(new DollarConverter(printerActor))
  case class dollar2euro(dollar: BigDecimal)
  case class dollar2bitcoin(dollar: BigDecimal)
}

class DollarConverter(printerActor: ActorRef) extends Actor with ActorLogging {
  import DollarConverter._
  import Printer._

  def receive = {
    case dollar2euro(dollar: BigDecimal) => val dollar2euro = dollar * 0.85
      printerActor ! PrintCurrency(dollar2euro)
    case dollar2bitcoin(dollar: BigDecimal) => val dollar2bitcoin = dollar/6500
      printerActor ! PrintCurrency(dollar2bitcoin)
    case _ => println("Error in DollarConverter.scala")
  }

}