/**
  * @author Rabit Ljatifi
  * Matrikelnummer: 1147409
  */

import akka.actor.{Actor, ActorLogging, ActorRef, Props}

object EuroConverter {
  def props(printerActor: ActorRef): Props = Props(new EuroConverter(printerActor))
  case class euro2bitcoin(euro: BigDecimal)
  case class euro2dollar(euro: BigDecimal)
}

class EuroConverter(printerActor: ActorRef) extends Actor with ActorLogging {
  import EuroConverter._
  import Printer._

  def receive = {
    case euro2dollar(euro: BigDecimal) => val euro2dollar = euro * 1.18
      printerActor ! PrintCurrency(euro2dollar)
    case euro2bitcoin(euro: BigDecimal) => val euro2bitcoin = euro / 5551
      printerActor ! PrintCurrency(euro2bitcoin)
    case _ => println("Error in EuroConverter.scala")
  }

}